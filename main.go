package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// TravelDestination representing destination to travel to
type TravelDestination struct {
	Location    string   `json:"location"`
	Images      []string `json:"images"`
	Description string   `json:"description"`
}

func allGiftBoxes(w http.ResponseWriter, r *http.Request) {
	// Read all directories stored in src/images folder
	files, err := ioutil.ReadDir("src/images")
	if err != nil {
		log.Fatal(err)
	}

	// From locations found in src/images, take images and create a gift box that will be used as
	// location package to travel
	gifts := []TravelDestination{}
	for _, file := range files {
		// Read images in folder
		images, err := ioutil.ReadDir("src/images/" + file.Name())
		if err != nil {
			log.Fatal(err)
		}

		gifts = append(gifts, TravelDestination{Location: file.Name()})

		// Append to last added location images from that folder
		for _, image := range images {
			gifts[len(gifts)-1].Images = append(gifts[len(gifts)-1].Images, image.Name())
		}

		// Get random short description from lorem ipsum
		resp, err := http.Get("https://loripsum.net/api/1/short/plaintext")
		if err != nil {
			log.Fatal(err)
		}

		description, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		// Before storing description, remove last 4 characters from the string
		// When the []byte is returned, lat 4 characters are: "; \n\n"
		gifts[len(gifts)-1].Description = string(description[:len(description)-4])
	}

	json.NewEncoder(w).Encode(gifts)
}

func handleRoutes() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/gifts", allGiftBoxes)

	log.Fatal(http.ListenAndServe(":8081", router))
}

func main() {
	handleRoutes()
}
